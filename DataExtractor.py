class DataExtractor:
    def __init__(self, data_file_path):
        self.data_file_path = data_file_path

    def extract_data(self, column1, column2, column3):
        f = open(self.data_file_path, "r")
        data = f.readlines()
        total_data_list = []
        for line in data[1:]:

            # print("line=",line)
            values_list = line.split()
            temp_list = []
            if values_list:
                if values_list[0][0].isdigit():
                    for item in values_list:
                        if item != "-":
                            temp_list.append(item)
                    total_data_list.append(temp_list)
        # print(total_data_list)
        final_data = []
        for item in total_data_list:
            final_data.append([item[column1], item[column2], item[column3]])
        return final_data
