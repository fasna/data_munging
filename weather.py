from DataExtractor import DataExtractor
from DataAnalyzer import DataAnalyzer


class Weather:
    def __init__(self, data_file_path):
        self.extractor = DataExtractor(data_file_path)
        self.analyzer = DataAnalyzer()

    def calculate_min_difference(self):
        data = self.extractor.extract_data(0, 1, 2)
        return self.analyzer.calculate_min_difference(data)


if __name__ == "__main__":
    weather = Weather("files/weather.dat")
    result = weather.calculate_min_difference()
    print(result)
