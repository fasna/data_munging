from DataExtractor import DataExtractor
from DataAnalyzer import DataAnalyzer


class Football:
    def __init__(self, data_file_path):
        self.extractor = DataExtractor(data_file_path)
        self.analyzer = DataAnalyzer()

    def calculate_min_difference(self):
        data = self.extractor.extract_data(1, 6, 7)
        return self.analyzer.calculate_min_difference(data)


if __name__ == "__main__":
    football = Football("files/football.dat")
    result = football.calculate_min_difference()
    print(result)
