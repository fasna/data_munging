class DataAnalyzer:
    def calculate_min_difference(self, data_list):
        min = 1000
        day_team = ""

        def calcu_min(item, min, day_team):
            if min > abs(int(item[1]) - int(item[2])):
                min = abs(int(item[1]) - int(item[2]))
                day_team = item[0]
            return (day_team, min)

        for item in data_list:
            if item[1].isdigit() and item[2].isdigit():
                day_team, min = calcu_min(item, min, day_team)
            elif item[1].isdigit():
                item[2] = item[2][0:2]
                day_team, min = calcu_min(item, min, day_team)
            else:
                item[1] = item[1][0:2]
                day_team, min = calcu_min(item, min, day_team)
        return day_team, min
